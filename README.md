https://uwyo.zoom.us/j/95927762290?pwd=Z05QY2RmYmhHYlEzdm11WFNkQW5VUT09
# Using Spatial R Workshop

This short workshop provides an introduction to using R and the spatial packages one can use for performing spatial analyses.  There are two parts to this workshop:

1.  Pre-recorded videos to assist you with becoming familiar with Base R, Tidyverse R and Code Repositories
2.  A live demonstration in which we will cover some of the functions to conduct spatial analyses. In this case we will learn to fit a simple species distribution model, using R Spatial to generate the data.frame for fitting the model. 

## Install Software
Before you begin, there are several pieces of software that need to be installed if you want to use R. I list the installation instructions here: [Workshop Software Installation](https://gitlab.com/salbeke/using-spatial-r-workshop/-/wikis/Installing-Program-R-and-other-software). 


## Prerequisite: 
If you are new to R, it is important to familiarize yourself with the code syntax and the development environment (R Studio) that we will be using during the live demonstration. I provide [multiple pre-recorded videos with demonstrations](https://gitlab.com/salbeke/using-spatial-r-workshop/-/wikis/home) of using the software. The content outline is as follows:

1. Download the Tutorial Data: [R_Intro.zip](https://pathfinder.arcc.uwyo.edu/wygisc/Albeke_Courses/UsingSpatialRWorkshop/R_Intro.zip)
2. Learn about using Code Repositories
3. Introduction to Base R
4. Introduction to R Tidyverse
5. Introduction to R Notebooks

## For the Live Demonstration
The live demonstration will have us working through a real world example. Below are instructions to be ready for all of the action we will be doing during the workshop!

1. Download the Demo Data: [R_SpatialExampleData.zip](https://pathfinder.arcc.uwyo.edu/wygisc/Albeke_Courses/IntroR_Spatial/R_SpatialExampleData.zip)
    - Once downloaded, please extract to a directory where you know the location. Ideally, you would be using your GitLab repository and an R project, thus a sub-directory of the project. However you go about it, you just need to know where the data live on your hard drive.
2. We need to include additional R Packages, sorry about that. Please install the following package:
    - _Metrics, ROCR, spatialEco, randomForest, rfUtilities_
3. Have R Studio and the R script found within the data opened.
